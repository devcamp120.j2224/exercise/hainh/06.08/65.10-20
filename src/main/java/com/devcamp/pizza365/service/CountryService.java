package com.devcamp.pizza365.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

@Service
public class CountryService {
	// tạo country
	@Autowired
	private CountryRepository countryRepository;    
    public CCountry createCountry(CCountry cCountry) {
		try {
			CCountry savedRole = countryRepository.save(cCountry);
			return savedRole;
		} catch (Exception e) {
            return null;
		}
	}

	// update country 

	public CCountry updateCountryService( Long id , CCountry cCountry){
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return savedCountry;
		} else {
			return null ;
		}
	}

	
}
