package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	// dùng để định nghĩa lại các hàm tìm kiếm , xài hàm có sẵn của Jparespository vẫn dc nhưng thiếu
	CCountry findByCountryCode(String countryCode);
	ArrayList<CCountry> findByCountryName(String countryName);

}
